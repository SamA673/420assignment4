class Address:

    # Constructor
    def __init__(self, name, street, city, province):

        # Validation...
        if not name or not street or not city or not province:
            raise ValueError("Please provide all necessary values to the Address constructor. [Expected: name, street, city, province]")
        
        self.name = name
        self.street = street
        self.city = city
        self.province = province

    # toString...
    def __str__(self):
        return f"{self.name}, {self.street}, {self.city}, {self.province}"

