from address import Address
from note import Note
from flask import Flask, abort, redirect, render_template, url_for
from markupsafe import escape

# Initialize the Flask app
app = Flask(__name__)


# ============================ Error handling  ============================= #
@app.errorhandler(404)
def page_not_found(e):
    return render_template('custom404.html'), 404

@app.errorhandler(400)
def bad_syntax(e):
    return render_template('custom400.html'), 400
# ============================ Home ============================= #

@app.route('/')
def home():
    return render_template('home.html')


# =========================== Notes ============================= #

notes = [
    Note(1, "Math notes"),
    Note(2, "Grocery list"),
    Note(3, "I like apples")
]


@app.route('/notes')
def display_notes():
    # Generate a bulleted list of addresses with links
    notes_list = "<ul>"
    for note in notes:

        note_url = url_for('display_notes')
        notes_list += f"""<li>
                                <a href={note_url}/{note.id}>  {str(note)} </a>
                            </li>"""
    notes_list += "</ul>"
    return notes_list


# Route to search for an address by name
@app.route('/notes/<id>')
def get_note(id):

    # Escape it so they can't inject code
    id = escape(id)

    # Checking if name is a number and redirecting to /notes if it's not...
    if not id.isdigit():
        return redirect('/notes')

    # Search for the given name in the list of notes...
    for note in notes:
        if note.id == int(id):
            return f"<p> {str(note)} </p>"  

    return redirect('/notes')

# =========================== Address book ============================= #

# Hardcoded list of addresses
addresses = [
    Address("Alex", "123 Main St", "NYC", "New York"),
    Address("Barbara", "456 Elm St", "Montreal", "Quebec"),
    Address("Charlie", "789 Oak St", "Toronto", "Ontario")
]


@app.route('/addressbook')
def addressbook():
    # Generate a bulleted list of addresses with links
    address_list = "<ul>"
    for address in addresses:

        address_url = url_for('addressbook')
        address_list += f"""<li>
                                <a href={address_url}/{address.name}>  {str(address)}</a>
                            </li>"""
    address_list += "</ul>"
    return address_list


# Route to search for an address by name
@app.route('/addressbook/<name>')
def get_address(name):

    # Escape it so they can't inject code
    name = escape(name)

    # Checking if name is an alnum... 
    if not name.isalnum():
        abort(400), 400  # 400 = bad user syntax 

    # Search for the given name in the list of addresses
    for address in addresses:
        if address.name == name:
            return f"<p>{escape(str(address))}</p>" 

    # If the address is not found, render the 404 error page and give code 404.
    abort(404), 404


# ===================================================================== #

if __name__ == '__main__':
    app.run(debug=True)
